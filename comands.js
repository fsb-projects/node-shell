var fs=require('fs')
comands={
    pwd:function (){
        return process.cwd()
    },
    date:function (){
       return new Date().toDateString()  
    },
    ls:function (){
        var files=fs.readdirSync('.')
        return files.map(f=>f.toString() + "\n")
        .reduce((total,element)=>total+element)
    },
    echo:function (string){
        return string=string.substr(4)=='echo'?string.substr(4):string
        
    },
    head:function (string){
        if (string.indexOf('.')==-1){
            string=string.substr(4)=='head'?string.substr(4).split('\n'):string.split('\n')
             
        }else{
            string=fs.readFileSync(string.substr(5),'utf8').split('\n')
        }
        var i=0
            var text=''
            while (string.length>0 && i<10){
                text+=string.shift()+'\n'
                i++
            }
            return text 
    },
    functionType:function (data){
        if (data.split('&').length>1){
            return '&'
        }else if (data.split('|').length>1){
            return '|'
        }else if(data=='pwd'){
            return 'pwd'    
        }else if(data=='date'){
            return 'date'
        }else if(data=='ls'){
            return 'ls'  
        }else if(data.slice(0,4)=='echo'){
            return 'echo'  
        }else if(data.slice(0,4)=='head'){
            return 'head'  
        }else{
            return 'no type'
        }
    }
    
}
function executor(data){
    var fn=comands.functionType(data)
    if (fn=='no type'){
        process.stdout.write(`no such a function "${data}"`)
    }else if (fn=='&'){
        data.split('&').map(e=>e.trim()).filter(e=>e!='').forEach(function(subCOm){
            executor(subCOm)
            process.stdout.write("\n")})
    }else if (fn=='|'){
        var pipeline=data.split('|')
        .map(e=>e.trim())
        .filter(e=>e!='')
        
        console.log(pipeline)
        var pipeline=pipeline.reduce(function(total, func) {
            return func!='no type'?comands[comands.functionType(func)](total):total
          },pipeline[0])
          process.stdout.write(pipeline)

    }else {
        
        process.stdout.write(comands[fn](data))
    }
}

module.exports={
executor
}